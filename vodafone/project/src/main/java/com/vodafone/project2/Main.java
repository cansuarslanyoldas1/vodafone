package com.vodafone.project2;

public class Main {

	public static void main(String[] args) {
		
		//Yemek ve İcecek eklemeyi adet bilgisi girmeksizin ekle metotlarıyla ekleyebilme.
		//Arkadaş grubu ile yemeğe çıktığımızda sırasıyla sipariş alınması önemli olmaksızın tüm yemek ve içecek siparişleri eklenir.
		//tüm yemekler ve tüm içecekler fiyat bilgisiyle faturada listelenir.
		Siparis siparis = new Siparis();
        siparis.yemekEkle(Yemek.SALATA);
        siparis.yemekEkle(Yemek.PIZZA);
        siparis.icecekEkle(Icecek.COLA);
        siparis.icecekEkle(Icecek.SU);
        siparis.icecekEkle(Icecek.COLA);

        System.out.println(siparis);

	}

}
