package com.vodafone.project2;

import java.util.ArrayList;
import java.util.List;

public class Siparis {
	private List<Icecek> icecekler;
    private List<Yemek> yemekler;

    public Siparis() {
        icecekler = new ArrayList<>();
        yemekler = new ArrayList<>();
    }

    public void icecekEkle(Icecek icecek) {
        icecekler.add(icecek);
    }

    public void yemekEkle(Yemek yemek) {
        yemekler.add(yemek);
    }

    public List<Icecek> getIcecekler() {
        return icecekler;
    }

    public List<Yemek> getYemekler() {
        return yemekler;
    }

    public double toplamFiyatHesapla() {
        double toplamFiyat = 0;
        for (Icecek icecek : icecekler) {
            toplamFiyat += icecek.getFiyat();
        }
        for (Yemek yemek : yemekler) {
            toplamFiyat += yemek.getFiyat();
        }
        return toplamFiyat;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(); //bellekte tek bir nesne yaratarak yeni nesne yaratmadan veriyi update etmek için builder kullanıldı.
        builder.append(yemekleriYazdir());
        builder.append(icecekleriYazdir());
        builder.append("\nToplam Fiyat: ").append(toplamFiyatHesapla()).append(" TL");
        return builder.toString();
    }
    
 // İçecekleri formatlı yazdırmak için..
    public String icecekleriYazdir() {
        StringBuilder builder = new StringBuilder();
        builder.append("\nIcecekler:\n");
        builder.append(addSembol());
        addSembol();
        for (Icecek icecek : icecekler) {
            builder.append(icecek.toString()).append(",\n");
        }
        return builder.toString();
    }
    //yemekler alt alta formatlı yazdırmak için toString kullanıldı
    public String yemekleriYazdir() {
        StringBuilder builder = new StringBuilder();
        builder.append("\nYemekler:\n");
        builder.append(addSembol());
        builder.append("\n");
        for (Yemek yemek : yemekler) {
            builder.append(yemek.toString()).append(",\n");
        }
        return builder.toString();
    }
    
    public static String addSembol() {
   	 	String line ="";
    	for(int i =0; i<40; i++) {
   		 line+="=";
   	 }
    	return line +"\n";
    }
}
