package com.vodafone.project2;

enum Icecek {
	
	COLA("Kola", 30.00),
    SU("Su", 15.0);
	private String ad;
    private double fiyat;

    Icecek(String ad, double fiyat) {
        this.ad = ad;
        this.fiyat = fiyat;
    }

    public String getAd() {
        return ad;
    }

    public double getFiyat() {
        return fiyat;
    }

    @Override
    public String toString() {
    	return String.format("%-15s %.2f TL", ad, fiyat);
    }
   
}
