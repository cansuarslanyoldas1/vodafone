package com.vodafone.project2;

enum Yemek {
    TAVUK_IZGARA("Tavuk Izgara", 150.0),
    SALATA("Salata", 110.90),
    PIZZA("Pizza", 180.0);
	
	private String ad;
    private double fiyat;

    Yemek(String ad, double fiyat) {
        this.ad = ad;
        this.fiyat = fiyat;
    }

    public String getAd() {
        return ad;
    }

    public double getFiyat() {
        return fiyat;
    }

    @Override
    public String toString() {
        return String.format("%-15s %.2f TL", ad, fiyat);
    }
}
