package com.vodafone.project;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		//Kalabalık bir şekilde gidilen bir restaurantta, sırasıyla sipariş alabilmek için bu yöntem yapıldı.
		//Yemek ve içecek sırasıyla girildiğinde kişi başı ödenecek tutar görünmesi iyi olabilirdi. 
		//Tavuk Izgara ve x3 kola sahibine ödemesi gereken ücretin gözükmesi kasa için kolaylaştırıcı olabilir.
		//2.yöntem için lütfen com.vodafone.project2 paketindeki main metodunu çalıştırınız.
		List<Siparis> siparisListesi = new ArrayList<>();
        siparisListesi.add(new Siparis(Yemek.TAVUK_IZGARA, 1, Icecek.KOLA,3));
        siparisListesi.add(new Siparis(Yemek.MAKARNA, 2, Icecek.SU, 2));
        siparisListesi.add(new Siparis(Yemek.YOK, 0, Icecek.SU, 1));
        double toplamFiyat = 0;

        for (Siparis siparis : siparisListesi) {
        	System.out.println(siparis);
            toplamFiyat += siparis.toplamFiyatHesapla();
        }
        addSembol();
        System.out.println(System.lineSeparator() +"Toplam Siparis Fiyati: " + toplamFiyat + " TL");
    }
 
	public static void addSembol() {
	 for(int i =0; i<40; i++) {
		 System.out.print("=");
	 }
 }
}
