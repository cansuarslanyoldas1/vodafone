package com.vodafone.project;

public class Siparis {
	private Icecek icecek;
    private Yemek yemek;
    private int icecekAdet;
    private int yemekAdet;

    public Siparis(Yemek yemek, int yemekAdet,Icecek icecek, int icecekAdet) {
        this.icecek = icecek;
        this.icecekAdet = icecekAdet;
        this.yemek = yemek;
        this.yemekAdet = yemekAdet;
    }

    public Icecek getIcecek() {
        return icecek;
    }

    public Yemek getYemek() {
        return yemek;
    }

    public int getIcecekAdet() {
        return icecekAdet;
    }

    public int getYemekAdet() {
        return yemekAdet;
    }

    public double toplamFiyatHesapla() {
        return (icecek.getFiyat() * icecekAdet) + (yemek.getFiyat() * yemekAdet);
    }
    
    @Override
    public String toString() {
        return "\nYemek: " + yemek + " x" + yemekAdet + "\nIcecek: " + icecek + " x" + icecekAdet + "\nToplam Fiyat: " + toplamFiyatHesapla() + " TL\n";
    }
}
