package com.vodafone.project;

enum Yemek {
	TAVUK_IZGARA("Hamburger", 150.0),
	MAKARNA("Hamburger", 120.0),
    PIZZA("Pizza", 100.0),
    YOK("Yok", 0.0);

    private String ad;
    private double fiyat;

    Yemek(String ad, double fiyat) {
        this.ad = ad;
        this.fiyat = fiyat;
    }

    public String getAd() {
        return ad;
    }

    public double getFiyat() {
        return fiyat;
    }
}
