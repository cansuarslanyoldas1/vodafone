package com.vodafone.project;

enum Icecek {
	KOLA("Kola", 15.0),
    SU("Su", 10.0),
    YOK("Yok", 0.0);

	private String ad;
    private double fiyat;

    Icecek(String ad, double fiyat) {
        this.ad = ad;
        this.fiyat = fiyat;
    }

    public String getAd() {
        return ad;
    }

    public double getFiyat() {
        return fiyat;
    }
}
